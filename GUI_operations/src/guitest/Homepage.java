package guitest;

//import javax.swing.JFrame;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;  
import java.awt.event.ActionListener;

import javax.swing.JFrame;  
import javax.swing.JLabel;  
import javax.swing.JOptionPane;  
import javax.swing.JTextField;
import javax.swing.JTabbedPane;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.border.Border;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JButton; 
import javax.swing.JScrollPane;  
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.BoxLayout;

import java.awt.Component;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.KeyEvent;
import java.awt.Font;  
import java.awt.Color;  


public class Homepage implements ActionListener{

	private JFrame frame;
	public JButton connectBtn,Btn1,Btn2,Btn3,Btn4,Btn5,Btn6,Btn7,Btn8,
	Btn9,Btn10,Btn11,Btn12,Btn13,Btn14,Btn15,Btn16,Btn17; 
	private JRadioButton rb1,rb2;
	
	 public static void main(String[] args) {  
         EventQueue.invokeLater(new Runnable() {  
              public void run() {  
                   try {  
                	   Homepage window = new Homepage();  
                        window.frame.setVisible(true);  
                   } catch (Exception e) {  
                        e.printStackTrace();  
                   }  
              }  
         });  
    }  
	public Homepage() {  
		  
//	         custListener = new CustListener();  
	
		 frame = new JFrame();  
         frame.setBounds(100, 100, 1040, 610);  
         frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  
         frame.getContentPane().setLayout(null);
         
         JTabbedPane tabbedPane = new JTabbedPane();
//         JTabbedPane tabbedPane2 = new JTabbedPane();
         
         
         
         JPanel panel1 = new JPanel();
         JPanel panel9 = new JPanel();
         Border blackline2 = BorderFactory.createTitledBorder("ISO 15693");
//       Border blackline3 = BorderFactory.createTitledBorder("ISO");
         Btn1 = new JButton("Inventory");  
         Btn1.setFont(new Font("Calibri", Font.PLAIN, 13));  
         Btn1.setForeground(Color.BLACK);
         Btn1.setBackground(Color.lightGray);
//		Btn1.setHorizontalAlignment(JButton.LEFT); 
 		Btn1.setEnabled(false);
//       connectBtn.addActionListener(custListener);  
 		Btn1.setFocusable(false);
 		
 		Btn5 = new JButton("Select");  
 		Btn5.setFont(new Font("Calibri", Font.PLAIN, 13));  
 		Btn5.setForeground(Color.BLACK);
 		Btn5.setBackground(Color.lightGray);
//		Btn1.setHorizontalAlignment(JButton.LEFT); 
 		Btn5.setEnabled(false);
//       connectBtn.addActionListener(custListener);  
 		Btn5.setFocusable(false);
 		
 		
 		Btn6 = new JButton("Quiet");  
 		Btn6.setFont(new Font("Calibri", Font.PLAIN, 13));  
 		Btn6.setForeground(Color.BLACK);
 		Btn6.setBackground(Color.lightGray);
//		Btn1.setHorizontalAlignment(JButton.LEFT); 
 		Btn6.setEnabled(false);
//       connectBtn.addActionListener(custListener);  
 		Btn6.setFocusable(false);
 		
 		
 		Btn7 = new JButton("Reset");  
 		Btn7.setFont(new Font("Calibri", Font.PLAIN, 13));  
 		Btn7.setForeground(Color.BLACK);
 		Btn7.setBackground(Color.lightGray);
//		Btn1.setHorizontalAlignment(JButton.LEFT); 
 		Btn7.setEnabled(false);
//       connectBtn.addActionListener(custListener);  
 		Btn7.setFocusable(false);
 		
 		
 		Btn8 = new JButton("Read Block(s)");  
 		Btn8.setFont(new Font("Calibri", Font.PLAIN, 13));  
 		Btn8.setForeground(Color.BLACK);
 		Btn8.setBackground(Color.lightGray);
//		Btn1.setHorizontalAlignment(JButton.LEFT); 
 		Btn8.setEnabled(false);
//       connectBtn.addActionListener(custListener);  
 		Btn8.setFocusable(false);
 		
 		
 		Btn9 = new JButton("Write Block(s)");  
 		Btn9.setFont(new Font("Calibri", Font.PLAIN, 13));  
 		Btn9.setForeground(Color.BLACK);
 		Btn9.setBackground(Color.lightGray);
//		Btn1.setHorizontalAlignment(JButton.LEFT); 
 		Btn9.setEnabled(false);
//       connectBtn.addActionListener(custListener);  
 		Btn9.setFocusable(false);
 		
 		
 		Btn10 = new JButton("Lock Block");  
 		Btn10.setFont(new Font("Calibri", Font.PLAIN, 13));  
 		Btn10.setForeground(Color.BLACK);
 		Btn10.setBackground(Color.lightGray);
//		Btn1.setHorizontalAlignment(JButton.LEFT); 
 		Btn10.setEnabled(false);
//       connectBtn.addActionListener(custListener);  
 		Btn10.setFocusable(false);
 		
 		
 		Btn11 = new JButton("Write AFI Flag");  
 		Btn11.setFont(new Font("Calibri", Font.PLAIN, 13));  
 		Btn11.setForeground(Color.BLACK);
 		Btn11.setBackground(Color.lightGray);
//		Btn1.setHorizontalAlignment(JButton.LEFT); 
 		Btn11.setEnabled(false);
//       connectBtn.addActionListener(custListener);  
 		Btn11.setFocusable(false);
 		
 		
 		Btn12 = new JButton("Lock AFI Flag");  
 		Btn12.setFont(new Font("Calibri", Font.PLAIN, 13));  
 		Btn12.setForeground(Color.BLACK);
 		Btn12.setBackground(Color.lightGray);
//		Btn1.setHorizontalAlignment(JButton.LEFT); 
 		Btn12.setEnabled(false);
//       connectBtn.addActionListener(custListener);  
 		Btn12.setFocusable(false);
 		
 		
 		Btn13 = new JButton("Write DSFID Flag");  
 		Btn13.setFont(new Font("Calibri", Font.PLAIN, 13));  
 		Btn13.setForeground(Color.BLACK);
 		Btn13.setBackground(Color.lightGray);
//		Btn1.setHorizontalAlignment(JButton.LEFT); 
 		Btn13.setEnabled(false);
//       connectBtn.addActionListener(custListener);  
 		Btn13.setFocusable(false);
 		
 		
 		Btn14 = new JButton("Lock DSFID Flag");  
 		Btn14.setFont(new Font("Calibri", Font.PLAIN, 13));  
 		Btn14.setForeground(Color.BLACK);
 		Btn14.setBackground(Color.lightGray);
//		Btn1.setHorizontalAlignment(JButton.LEFT); 
 		Btn14.setEnabled(false);
//       connectBtn.addActionListener(custListener);  
 		Btn14.setFocusable(false);
 		
 		
 		Btn15 = new JButton("Get System Info");  
 		Btn15.setFont(new Font("Calibri", Font.PLAIN, 13));  
 		Btn15.setForeground(Color.BLACK);
 		Btn15.setBackground(Color.lightGray);
//		Btn1.setHorizontalAlignment(JButton.LEFT); 
 		Btn15.setEnabled(false);
//       connectBtn.addActionListener(custListener);  
 		Btn15.setFocusable(false);
 		
 		
 		Btn16 = new JButton("Block Security Status");  
 		Btn16.setFont(new Font("Calibri", Font.PLAIN, 13));  
 		Btn16.setForeground(Color.BLACK);
 		Btn16.setBackground(Color.lightGray);
//		Btn1.setHorizontalAlignment(JButton.LEFT); 
 		Btn16.setEnabled(false);
//       connectBtn.addActionListener(custListener);  
 		Btn16.setFocusable(false);
 		
 		
 		Btn17 = new JButton("Full Inventory");  
 		Btn17.setFont(new Font("Calibri", Font.PLAIN, 13));  
 		Btn17.setForeground(Color.BLACK);
 		Btn17.setBackground(Color.lightGray);
//		Btn1.setHorizontalAlignment(JButton.LEFT); 
 		Btn17.setEnabled(false);
//       connectBtn.addActionListener(custListener);  
 		Btn17.setFocusable(false);
 		
 		
//		Btn1.setBounds(40, 80, 90, 100);
 		if (Btn5.isEnabled()) {
 			Border border2 = BorderFactory.createLineBorder(Color.BLUE,2);
 			Btn5.setBorder(border2);
 			}
//    JLabel filler = new JLabel("Tab 1");
//    filler.setHorizontalAlignment(JLabel.CENTER);
         panel1.add(Btn1);
      //   panel1.add(Box.createRigidArea(new Dimension(0, 80)));
         panel1.add(Box.createVerticalStrut(5));
         panel1.add(Btn5);
         panel1.add(Box.createVerticalStrut(5));
         panel1.add(Btn6);
         panel1.add(Box.createVerticalStrut(5));
         panel1.add(Btn7);
         panel1.add(Box.createVerticalStrut(5));
         panel1.add(Btn8);
         panel1.add(Box.createVerticalStrut(5));
         panel1.add(Btn9);
         panel1.add(Box.createVerticalStrut(5));
         panel1.add(Btn10);
         panel1.add(Box.createVerticalStrut(5));
         panel1.add(Btn11);
         panel1.add(Box.createVerticalStrut(5));
         panel1.add(Btn12);
         panel1.add(Box.createVerticalStrut(5));
         panel1.add(Btn13);
         panel1.add(Box.createVerticalStrut(5));
         panel1.add(Btn14);
         panel1.add(Box.createVerticalStrut(5));
         panel1.add(Btn15);
         panel1.add(Box.createVerticalStrut(5));
         panel1.add(Btn16);
         panel1.add(Box.createVerticalStrut(5));
         panel1.add(Btn17);
//    panel1.add(Box.createVerticalGlue());
         
         panel1.setBorder(blackline2);
         BoxLayout layout1 = new BoxLayout(panel1, BoxLayout.Y_AXIS);
         panel1.setLayout(layout1);
         Btn1.setAlignmentX(Component.LEFT_ALIGNMENT);
         Btn5.setAlignmentX(Component.LEFT_ALIGNMENT);
         Btn6.setAlignmentX(Component.LEFT_ALIGNMENT);
         Btn7.setAlignmentX(Component.LEFT_ALIGNMENT);
         Btn8.setAlignmentX(Component.LEFT_ALIGNMENT);
         Btn9.setAlignmentX(Component.LEFT_ALIGNMENT);
         Btn10.setAlignmentX(Component.LEFT_ALIGNMENT);
         Btn11.setAlignmentX(Component.LEFT_ALIGNMENT);
         Btn12.setAlignmentX(Component.LEFT_ALIGNMENT);
         Btn13.setAlignmentX(Component.LEFT_ALIGNMENT);
         Btn14.setAlignmentX(Component.LEFT_ALIGNMENT);
         Btn15.setAlignmentX(Component.LEFT_ALIGNMENT);
         Btn16.setAlignmentX(Component.LEFT_ALIGNMENT);
         Btn17.setAlignmentX(Component.LEFT_ALIGNMENT);
        
         JPanel panel6 = new JPanel();
         Border blackline3 = BorderFactory.createTitledBorder("ICODE");
//       Border blackline3 = BorderFactory.createTitledBorder("ISO");
         Btn2 = new JButton("Read EAS");  
         Btn2.setFont(new Font("Calibri", Font.PLAIN, 13));  
         Btn2.setForeground(Color.BLACK);
         Btn2.setBackground(Color.lightGray);
//		Btn1.setHorizontalAlignment(JButton.LEFT); 
         Btn2.setEnabled(false);
//       connectBtn.addActionListener(custListener);  
         Btn2.setFocusable(false);
 		if (Btn2.isEnabled()) {
 			Border border2 = BorderFactory.createLineBorder(Color.BLUE,2);
 			Btn2.setBorder(border2);
 			}
//    JLabel filler = new JLabel("Tab 1");
//    filler.setHorizontalAlignment(JLabel.CENTER);
         panel6.add(Btn2);
         panel6.setBorder(blackline3);
         BoxLayout layout2 = new BoxLayout(panel6, BoxLayout.Y_AXIS);
         panel6.setLayout(layout2);
         Btn2.setAlignmentX(Component.CENTER_ALIGNMENT);
         
         
         JPanel panel7 = new JPanel();
         Border blackline4 = BorderFactory.createTitledBorder("TAG IT");
         Btn3 = new JButton("Write Two Blocks");  
         Btn3.setFont(new Font("Calibri", Font.PLAIN, 13));  
         Btn3.setForeground(Color.BLACK);
         Btn3.setBackground(Color.lightGray);
//		Btn1.setHorizontalAlignment(JButton.LEFT); 
         Btn3.setEnabled(false);
//       connectBtn.addActionListener(custListener);  
         Btn3.setFocusable(false);
 		if (Btn3.isEnabled()) {
 			Border border2 = BorderFactory.createLineBorder(Color.BLUE,2);
 			Btn3.setBorder(border2);
 			}
//    JLabel filler = new JLabel("Tab 1");
//    filler.setHorizontalAlignment(JLabel.CENTER);
         panel7.add(Btn3);
         panel7.setBorder(blackline4);
         BoxLayout layout3 = new BoxLayout(panel7, BoxLayout.Y_AXIS);
         panel7.setLayout(layout3);
         Btn3.setAlignmentX(Component.CENTER_ALIGNMENT);
         
         
         JPanel panel8 = new JPanel();
         Border blackline5 = BorderFactory.createTitledBorder("AUTO INVENTORY");
         Btn4 = new JButton("15K AUTO ON");  
         Btn4.setFont(new Font("Calibri", Font.PLAIN, 13));  
         Btn4.setForeground(Color.BLACK);
         Btn4.setBackground(Color.lightGray);
//		Btn1.setHorizontalAlignment(JButton.LEFT); 
         Btn4.setEnabled(false);
//       connectBtn.addActionListener(custListener);  
         Btn4.setFocusable(false);
 		if (Btn4.isEnabled()) {
 			Border border2 = BorderFactory.createLineBorder(Color.BLUE,2);
 			Btn4.setBorder(border2);
 			}
//    JLabel filler = new JLabel("Tab 1");
//    filler.setHorizontalAlignment(JLabel.CENTER);
         panel8.add(Btn4);
         panel8.setBorder(blackline5);
         BoxLayout layout4 = new BoxLayout(panel8, BoxLayout.Y_AXIS);
         panel8.setLayout(layout4);
         Btn4.setAlignmentX(Component.RIGHT_ALIGNMENT);
         
         
//     panel1.setAlignmentX(0);;
//     panel1.setBounds(25, 80, 50, 150);
//         BoxLayout layout5 = new BoxLayout(panel9, BoxLayout.X_AXIS);
         panel9.setLayout(new FlowLayout());
         panel9.add(panel1);
         panel9.add(Box.createHorizontalStrut(5));
         panel9.add(panel6);
         panel9.add(Box.createHorizontalStrut(5));
         panel9.add(panel7);
         panel9.add(Box.createHorizontalStrut(5));
         panel9.add(panel8);
         tabbedPane.addTab("ISO 15693", null, panel9,"Tab 1 tooltip");
//    tabbedPane.addTab("ISO 15693", null, panel2,"Tab 1 tooltip"); 
         tabbedPane.setMnemonicAt(0, KeyEvent.VK_1);
         
         
         JPanel panel2 = new JPanel(false);
         JLabel filler2 = new JLabel("Tab 2");
         filler2.setHorizontalAlignment(JLabel.CENTER);
//     panel2.setBounds(15, 30, 200, 200);
         panel2.add(filler2);
         tabbedPane.addTab("ISO 14443", null, panel2,"Tab 2 tooltip");
         tabbedPane.setMnemonicAt(0, KeyEvent.VK_2); 
         tabbedPane.setBounds(5, 65, 650, 590);
         
         
         JPanel panel3 = new JPanel(true);
         JLabel filler3 = new JLabel("Tab 3");
        filler3.setHorizontalAlignment(JLabel.CENTER);
//     panel1.setBounds(15, 30, 200, 200);
         panel3.add(filler3);
         tabbedPane.addTab("System Level Commands", null, panel3,"Tab 3 tooltip");
         tabbedPane.setMnemonicAt(0, KeyEvent.VK_3);
         tabbedPane.setFont(new Font("Calibri", Font.PLAIN, 13));
         frame.getContentPane().add(tabbedPane);
         
         
         JPanel panel4 = new JPanel(false);
         Border blackline = BorderFactory.createTitledBorder("Connection Type");
         rb1 = new JRadioButton("USB");
         rb2 = new JRadioButton("TCP/IP");
         ButtonGroup bg=new ButtonGroup();    
         bg.add(rb1);bg.add(rb2);
         rb1.setActionCommand("rb1");
         rb2.setActionCommand("rb2");
//    rb1.addActionListener(this);
         rb2.addActionListener(this);
//     filler2.setHorizontalAlignment(JLabel.CENTER);
//     panel2.setBounds(15, 30, 200, 200);
         panel4.add(rb1);
//      panel4.add(rb2);
//     panel1.add(new JLabel(spaces + "Title border to JPanel" + spaces));  
         panel4.setBorder(blackline);
//         tabbedPane2.addTab("Connection Type", null, panel4,"Tab 1 tooltip");
//         tabbedPane2.setMnemonicAt(0, KeyEvent.VK_4); 
//         tabbedPane2.setBounds(150, 10, 180, 50);
         rb1.setFont(new Font("Calibri", Font.PLAIN, 13));
         rb2.setFont(new Font("Calibri", Font.PLAIN, 13));
         panel4.setBounds(150, 10, 160, 52);
         frame.getContentPane().add(panel4);
         
         
         JTextArea textArea = new JTextArea();  
         JScrollPane scrollableTextArea = new JScrollPane(textArea);  
   
         scrollableTextArea.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);  
         scrollableTextArea.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);  
         scrollableTextArea.setBounds(660, 87, 365, 465);
         frame.getContentPane().add(scrollableTextArea);  
         
		connectBtn = new JButton("Connect");  
        connectBtn.setFont(new Font("Calibri", Font.PLAIN, 13));  
//       connectBtn.setForeground(Color.gray);
		connectBtn.setBackground(Color.lightGray);
		Border border = BorderFactory.createLineBorder(Color.BLUE,2);
		connectBtn.setBorder(border);
        connectBtn.setBounds(15, 20, 110, 22);  
//       connectBtn.addActionListener(custListener);  
        connectBtn.setFocusable(false);
         
         frame.getContentPane().add(connectBtn); 
         frame.setLocationRelativeTo(null);  
         frame.setResizable(false);  
         frame.setTitle("Test Application");
}
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(rb1.isSelected()){    
		//	JOptionPane.showMessageDialog(this,"You are Male.");    
			}    
		if(rb2.isSelected()){    
				 JPanel panel5 = new JPanel(false);
				 JLabel filler4 = new JLabel("Reader IP");
				 JTextField tf1=new JTextField("192.168.1.200");
				 JLabel filler5 = new JLabel("Server Port");
				 JTextField tf2=new JTextField("9090");
				 JLabel filler6 = new JLabel("Client Port");
				 JTextField tf3=new JTextField("9000");
		         Border blackline1 = BorderFactory.createTitledBorder("TCP Connection");
		         
		         panel5.add(filler4);
		         panel5.add(tf1);
		         panel5.add(filler5);
		         panel5.add(tf2);
		         panel5.add(filler6);
		         panel5.add(tf3);
		         panel5.setBorder(blackline1);
		         panel5.setBounds(350, 10, 260, 82);
		         frame.getContentPane().add(panel5);
			}    
	}
}