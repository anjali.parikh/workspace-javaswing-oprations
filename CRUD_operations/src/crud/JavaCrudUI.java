package crud;
//import java.awt.event.*;
import java.awt.EventQueue;  
import java.awt.event.ActionEvent;  
import java.awt.event.ActionListener;  
import javax.swing.JFrame;  
import javax.swing.JLabel;  
import javax.swing.JOptionPane;  
import javax.swing.JTextField;  
//import crud.Customer;  
//import crud.CustomerDelete;  
import javax.swing.JButton;  
import java.awt.Font;  
import java.awt.Color;  


public class JavaCrudUI {
    private JFrame frame;  
    private JTextField custIdTF;  
    private JTextField custNameTF;  
    private JTextField emailIdTF;  
    private JTextField addressTF;  
    private JButton addBtn;  
    private JButton updateBtn;  
    private JButton deleteBtn;  
    private CustListener custListener;  
  //  private CustomerDelete custDA;  
      
    /**  
     * Launch the application.  
     */  
    public static void main(String[] args) {  
         EventQueue.invokeLater(new Runnable() {  
              public void run() {  
                   try {  
                        JavaCrudUI window = new JavaCrudUI();  
                        window.frame.setVisible(true);  
                   } catch (Exception e) {  
                        e.printStackTrace();  
                   }  
              }  
         });  
    }  
 
  
    public JavaCrudUI() {  
   //      custDA = new CustomerDelete();  
         custListener = new CustListener();  
         initialize();  
 //        updateCustomerForm();  
           
//          Set text fields to read mode  
  //       areTextFieldsEditable(false);  
    }  
 
  
    private void initialize() {  
         frame = new JFrame();  
         frame.setBounds(100, 100, 570, 300);  
         frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  
         frame.getContentPane().setLayout(null);  
           
         JLabel custIdLbl = new JLabel("Customer ID:");  
         custIdLbl.setBounds(58, 10, 76, 16);  
         frame.getContentPane().add(custIdLbl);  
           
         custIdTF = new JTextField();  
         custIdTF.setBounds(141, 7, 376, 22);  
         frame.getContentPane().add(custIdTF);  
         custIdTF.setColumns(10);  
         custIdTF.setEditable(false);  
           
         JLabel custNameLbl = new JLabel("Customer name:");  
         custNameLbl.setBounds(38, 39, 96, 16);  
         frame.getContentPane().add(custNameLbl);  
           
         custNameTF = new JTextField();  
         custNameTF.setBounds(141, 36, 376, 22);  
         custNameTF.setColumns(10);  
         frame.getContentPane().add(custNameTF);  
           
         JLabel emailIdLbl = new JLabel("EmailId:");  
         emailIdLbl.setBounds(82, 68, 52, 16);  
         frame.getContentPane().add(emailIdLbl);  
           
         emailIdTF = new JTextField();  
         emailIdTF.setBounds(141, 65, 376, 22);  
         emailIdTF.setColumns(10);  
         frame.getContentPane().add(emailIdTF);  
           
         JLabel addressLbl = new JLabel("Address:");  
         addressLbl.setBounds(83, 97, 51, 16);  
         frame.getContentPane().add(addressLbl);  
           
         addressTF = new JTextField();  
         addressTF.setBounds(141, 94, 376, 22);  
         addressTF.setColumns(10);  
         frame.getContentPane().add(addressTF);  
           
           
         addBtn = new JButton("ADD");  
         addBtn.setFont(new Font("Tahoma", Font.BOLD, 13));  
         addBtn.setForeground(Color.BLUE);
         addBtn.setBounds(15, 228, 173, 25);  
         addBtn.addActionListener(custListener);  
         frame.getContentPane().add(addBtn);  
           
         updateBtn = new JButton("UPDATE");  
         updateBtn.setFont(new Font("Tahoma", Font.BOLD, 13)); 
         updateBtn.setForeground(Color.BLUE);
         updateBtn.setBounds(200, 228, 166, 25);  
         updateBtn.addActionListener(custListener);  
         frame.getContentPane().add(updateBtn);  
           
         deleteBtn = new JButton("DELETE");  
         deleteBtn.setFont(new Font("Tahoma", Font.BOLD, 13));  
         deleteBtn.setForeground(Color.BLUE);
         deleteBtn.setBounds(378, 228, 173, 25);  
         deleteBtn.addActionListener(custListener);  
         frame.getContentPane().add(deleteBtn);  
           
         frame.setLocationRelativeTo(null);  
         frame.setResizable(false);  
         frame.setTitle("Customer Management");  
    }  
      

//    private void updateCustomerForm() {  
//         Customer cust = custDA.getCurrentCustomer();  
//           
//         custIdTF.setText(cust.getCustId());  
//         custNameTF.setText(cust.getCustName());  
//         emailIdTF.setText(cust.getEmailId());  
//         addressTF.setText(cust.getAddress());  
//       
//    }  
      

//    private void areTextFieldsEditable(boolean flag) {  
//         custIdTF.setEditable(flag);  
//         custNameTF.setEditable(flag);  
//         emailIdTF.setEditable(flag);  
//         addressTF.setEditable(flag);  
//    
//    }  
      

    class CustListener implements ActionListener {  
 
         @Override  
         public void actionPerformed(ActionEvent ev) {  
              String ac = ev.getActionCommand();  
                
              switch(ac) {  
              case "ADD": setEditMode("ADD"); break;  
              case "UPDATE": setEditMode("UPDATE"); break;  
              case "SAVE ADD": addCust();   
                                   resetEditMode(); break;  
              case "SAVE UPDATE": updateCust();   
                                       resetEditMode(); break;  
              case "CANCEL": resetEditMode(); break;  
              case "DELETE": deleteCust(); break;  
              }  
         }  
 

         private void resetEditMode() {  
                
     //         areTextFieldsEditable(false);  
              addBtn.setEnabled(true);  
              updateBtn.setEnabled(true);  
              deleteBtn.setEnabled(true);  
                
    //          updateCustomerForm();  
         }  
           

         private void setEditMode(String mode) {                 
    //          areTextFieldsEditable(true);  
                
              if(mode.equals("ADD")) clearForm();  
              else if(mode.equals("UPDATE")) custIdTF.setEditable(false);  
                     
              addBtn.setEnabled(false);  
              updateBtn.setEnabled(false);  
              deleteBtn.setEnabled(false);  
         }  
           
         private void clearForm() {  
              custIdTF.setText("");  
              custNameTF.setText("");  
              emailIdTF.setText("");  
              addressTF.setText("");  
         }  
 
 
         private void deleteCust() {  
              int res = JOptionPane.showConfirmDialog(null, "Are you sure you want to delete customer?", "",   
                        JOptionPane.YES_NO_OPTION);  
                
              if(res == 0) {  
  //                 custDA.deleteCustomer(custIdTF.getText());  
  //                 updateCustomerForm();  
              }  
         }  
 

         private void updateCust() {  
//              custDA.updateCustomer(custIdTF.getText(),  
//                                            new Customer(  
//                                            custIdTF.getText(),  
//                                            custNameTF.getText(),  
//                                            emailIdTF.getText(),  
//                                            addressTF.getText())  
//                                            );  
//              updateCustomerForm();  
         }  
 
 
         private void addCust() {  
//              custDA.addCustomer(     new Customer(  
//                                       custIdTF.getText(),  
//                                       custNameTF.getText(),  
//                                       emailIdTF.getText(),  
//                                       addressTF.getText())  
//                                 );  
//              updateCustomerForm();  
         }  
           
    }  
      
}  

