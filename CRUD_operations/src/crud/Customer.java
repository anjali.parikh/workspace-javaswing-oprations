package crud;

public class Customer {
	 private String custId,custName,emailId,address;  
 
public Customer(String custId, String studName, String emailId, String address) {  
this.custId = custId;  
this.custName = studName;  
this.emailId = emailId;  
this.address = address;  
}  


public void setCustId(String custId) {  
this.custId = custId;  
}  

public void setCustName(String custName) {  
this.custName = custName;  
}  

public void setEmailId(String emailId) {  
this.emailId = emailId;  
}  


public void setAddress(String address) {  
this.address = address;  
}  


public String getCustId() {  
return custId;  
}  

public String getCustName() {  
return custName;  
}  

public String getEmailId() {  
return emailId;  
}  

public String getAddress() {  
return address;  
}  

}
