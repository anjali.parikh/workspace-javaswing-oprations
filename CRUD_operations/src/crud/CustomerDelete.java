package crud;

import java.sql.*;  
import java.util.ArrayList;  
import javax.swing.JOptionPane;  
//import crud.Customer; 

public class CustomerDelete {
	
	private ArrayList<Customer> customerList;  
    private int recordCursor;  
      
    private Connection connection;  
  
    private final String dbDir = "..\\CRUD_operations\\SampleDB.db";  
          public CustomerDelete() {                 
 
         setRecordCursorAtStart();  
         setConnection();  
         populateCustomerList();  
    }  
 
    public void setRecordCursorAtStart() {  
         recordCursor = 0;  
    }  
      
       private void setConnection() {  
         try {  
  
              Class.forName("org.sqlite.JDBC");  
              connection = DriverManager.getConnection("jdbc:sqlite:" + dbDir);  
                
         } catch (ClassNotFoundException | SQLException e) {  
              // TODO Auto-generated catch block  
              e.printStackTrace();  
         }  
           
    }  
      
     private void populateCustomerList() {  
         customerList = new ArrayList<Customer>();  
           
         try {                 
              PreparedStatement ps = connection.prepareStatement("SELECT * FROM customer");  
              ResultSet rs = ps.executeQuery();  
                
              Customer cust;  
              while(rs.next()) {  
                     

                   if(rs.getString("stamp").contains("inactive"))  
                        continue;  
                     
                   cust = new Customer(rs.getString("custId"),  
                                              rs.getString("name"),  
                                              rs.getString("emailId"),  
                                              rs.getString("address"));  
                     
                   customerList.add(cust);  
              }  
                
         } catch (SQLException e) {  
              // TODO Auto-generated catch block  
              e.printStackTrace();  
         }  
    }  
      
 
    private void commitStatement(String query) {  
         try {       
       
              PreparedStatement ps = connection.prepareStatement(query);  
              ps.executeUpdate();  
                
         } catch (SQLException e) {  
              JOptionPane.showMessageDialog(null, "An error occured due invalid/conflict in customer data inputted.", "", JOptionPane.ERROR_MESSAGE);  
              e.printStackTrace();  
         }            
           
         populateCustomerList();  
    }  
 

    public void addCustomer(Customer cust) {  
         commitStatement(String.format("INSERT INTO customer VALUES ('%s', '%s', '%s', '%s', '%s', 'active')",  
                                             cust.getCustId(),  
                                             cust.getCustName(),  
                                             cust.getEmailId(),  
                                             cust.getAddress()  
                                           ));          
         setRecordCursorAtEnd();  
    }  
      
    public void updateCustomer(String oldCustId, Customer cust) {  
         commitStatement(String.format("UPDATE customer SET custId = '%s', name = '%s', payterm = '%s', address = '%s', company = '%s' WHERE custId = '%s'",  
                                             cust.getCustId(),  
                                             cust.getCustName(),  
                                             cust.getEmailId(),  
                                             cust.getAddress(),    
                                             oldCustId));  
    }  
      
    public void deleteCustomer(String custId) {  
         commitStatement(String.format("UPDATE customer SET stamp = 'inactive' WHERE custId = '%s'", custId));  
         setRecordCursorAtStart();  
    }  
 
    public Customer getCurrentCustomer() {  
         return customerList.get(recordCursor);  
    }  
      
    public void setRecordCursorAtEnd() {  
         recordCursor = customerList.size()-1;  
    }  
            
    public ArrayList<Customer> getCustomerList() {  
         return customerList;  
    }       
      

}
